﻿using API.Context;
using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Mapper
{
    public static class Map
    {

        public static Student DtoToEntity(StudentDTO dto)
        {
            return new Student()
            {
                Id = dto.Id,
                PN = dto.PN,
                Name = dto.Name,
                LastName = dto.LastName,
                BirthDate = dto.BirthDate,
                Gender = dto.Gender

            };
        }
        public static StudentDTO EntityToDto(Student entity)
        {
            return new StudentDTO()
            {
                Id = entity.Id,
                PN = entity.PN,
                Name = entity.Name,
                LastName = entity.LastName,
                BirthDate = entity.BirthDate,
                Gender = entity.Gender

            };
        }

        public static IEnumerable<StudentDTO> EntityToDto(List<Student> entities)
        {
            foreach(var entity in entities)
            {
                yield return EntityToDto(entity);
            }

        }
    }
}
