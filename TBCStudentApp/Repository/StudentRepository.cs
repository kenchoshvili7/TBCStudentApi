﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Context;
using API.Models;


namespace API.Repository
{
    public class StudentRepository : IStudentRepository
    {
        //private readonly IServiceScope _scope;
        private readonly StudentDbContext _dbContext;

        //public StudentRepository(IServiceProvider service)
        //{
        //    _scope = service.CreateScope();
        //    _dbContext = _scope.ServiceProvider.GetRequiredService<StudentDbContext>();

        //}
        public StudentRepository(StudentDbContext context)
        {
            
            _dbContext = context;

        }
        
        public async Task<bool> CreateAsync(Student model)
        {
            return await Task.Run(async () =>
            {
                var succes = false;
                _dbContext.Students.Add(model);

                var numberOfItemsCreated = await _dbContext.SaveChangesAsync();
                if (numberOfItemsCreated == 1)
                    succes = true;

                return succes;
            });


        }
        public async Task<bool> DeleteAsync(int id)
        {
            return await Task.Run(async () =>
            {
                var success = false;
                var entity = _dbContext.Students.SingleOrDefault(s => s.Id == id);
                if (entity != null)
                    _dbContext.Remove(entity);

                var numberOfItemsCreated = await _dbContext.SaveChangesAsync();
                if (numberOfItemsCreated == 1)
                    success = true;

                return success;
            });
        }
        public async Task<List<Student>> GetAllAsync(string schtxt)
        {
            return await Task.Run(() =>
            {
                IQueryable<Student> result = _dbContext.Students;
                if (!string.IsNullOrEmpty(schtxt))
                    result = result.Where(s => s.PN.Contains(schtxt));

              

                return result.ToList();
            });
        }
        public async Task<Student> GetByIDAsync(int id)
        {
            return await Task.Run(() =>
              {
                  return  _dbContext.Students.SingleOrDefault(x => x.Id == id);
              });

        }
        public async Task<bool> UpdateAsync(Student model)
        {

            return await Task.Run(async () =>
            {
                var success = false;
                var entity = _dbContext.Students.SingleOrDefault(s => s.Id == model.Id);
                if (entity != null)
                {
                    entity.PN = model.PN;
                    entity.Name = model.Name;
                    entity.LastName = model.LastName;
                    entity.BirthDate = model.BirthDate;
                    entity.Gender = model.Gender;
                }


                var numberOfItemsCreated = await _dbContext.SaveChangesAsync();
                if (numberOfItemsCreated == 1)
                    success = true;

                return success;
            });
        }
    }
}
