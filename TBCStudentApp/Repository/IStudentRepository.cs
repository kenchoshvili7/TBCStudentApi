﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Context;


namespace API.Repository
{
    public interface IStudentRepository
    {
        Task<bool> CreateAsync(Student model);
        Task<bool> UpdateAsync(Student model);
        Task<bool> DeleteAsync(int id); 
        Task<Student> GetByIDAsync(int id);
        Task<List<Student>> GetAllAsync(string schtxt);
    }
}
