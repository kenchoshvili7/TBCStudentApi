﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Service;
using API.Models;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {

        private readonly IStudentService _service;

        public StudentController(IStudentService service)
        {
            this._service = service;
        }


        [HttpPost]
        public async Task<Response<StudentDTO>> Post([FromBody] StudentDTO model)
        {
            return await _service.CreateAsync(model);
        }

        [HttpPut]
        [Route("updateStudent")]
        public async Task<Response<StudentDTO>> Update([FromBody] StudentDTO model)
        {
            return await _service.UpdateAsync(model);
        }


        [HttpDelete]
        [Route("deleteStudent")]
        public async Task<bool> Delete(int id)
        {
            return  await _service.DeleteAsync(id);
        }


        [HttpGet]
        [Route("all")]
        public async Task<IEnumerable<StudentDTO>> Get(string srch,int currentPageIndex=0)
        {
            return await _service.GetAllAsync(srch, currentPageIndex);
        }

        [HttpGet]
        [Route("pagingSettings")]
        public async Task<PagingSettings> GetPagingSettings(string srch, int currentPageIndex = 0)
        {
            return await _service.GetPagingSettings(srch, currentPageIndex);
        }

        [HttpGet]
        [Route("detail")]
        public async Task<StudentDTO> GetDetail(int id)
        {
            return await _service.GetByIDAsync(id);
        }


        [HttpGet]
        [Route("ping")]
        public async Task<string> Ping() {

            return "Ping OK!";
        }

    }
}
