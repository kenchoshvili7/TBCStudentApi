﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class StudentDTO
    {



        public int Id { get; set; }

        public string PN { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public int Gender { get; set; }









    }



    public class PagingSettings
    {

        public PagingSettings()
        {
            PageCountList = new List<int>();
        }

        public bool showFirstButtton { get; set; }

        public bool showPrevButton { get; set; }

        public bool showNextButton { get; set; }

        public bool showLastButton { get; set; }


        public int currentPageIndex { get; set; }
        public int firstPageIndex { get; set; }
        public int prevPageIndex { get; set; }
        public int nextPageIndex { get; set; }
        public int lastPageIndex { get; set; }



        public List<int> PageCountList { get; set; }
    }
}
