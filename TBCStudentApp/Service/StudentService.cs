﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Repository;
using API.CustmHelpers;

namespace API.Service
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository _repository;

        public StudentService(IStudentRepository repository)
        {
            _repository = repository;
        }

        #region Validations
        public async Task<bool> ValidatePNAsync(string pn, int id)
        {
            return await Task.Run(() =>
            {
                return _repository.GetAllAsync(null).Result.Where(s => s.PN == pn && s.Id != id).Any();
            });
        }

        public async Task<bool> ValidateAgeLessThan16Async(DateTime birthdate)
        {
            return await Task.Run(() =>
            {
                var isValidRange = false;
                var age = DateTime.Now.Year - birthdate.Year;

                if (age > 16)
                    isValidRange = true;
                return isValidRange;
            });
        }

        #endregion 

        public async Task<Response<StudentDTO>> CreateAsync(StudentDTO model)
        {
            return await Task.Run(async () =>
            {

                Response<StudentDTO> response = new Response<StudentDTO>();
                response.Success = false;

                var pnAlreadyExist = await ValidatePNAsync(model.PN,model.Id);
                if(pnAlreadyExist)
                    response.Message = "პირადი ნომრით უზერი უკვე არსებობს \n";


                var ageIsAbove16 = await ValidateAgeLessThan16Async(model.BirthDate);
                if (!ageIsAbove16)
                    response.Message += "ასაკი ნაკლებია 16 ზე";


                if(!pnAlreadyExist && ageIsAbove16)
                {
                    var success = await _repository.CreateAsync(Mapper.Map.DtoToEntity(model));
                    if (success)
                    {
                        response.Body = model;
                        response.Success = true;
                        return response;
                    }
                }
              
                return response;

            });

        }
        public async Task<bool> DeleteAsync(int id)
        {
            return await _repository.DeleteAsync(id);
        }
        public async Task<List<StudentDTO>> GetAllAsync(string srchtxt,int currentPageIndex=0)
        {
            return   PagingHelper.GetOnePageData(Mapper.Map.EntityToDto(await _repository.GetAllAsync(srchtxt)).ToList(),currentPageIndex);
        }

        public async Task<PagingSettings> GetPagingSettings(string srchtxt, int currentPageIndex = 0)
        {
            return PagingHelper.Get_Paging_Settngs(Mapper.Map.EntityToDto(await _repository.GetAllAsync(srchtxt)).ToList(), currentPageIndex);
        }


        public async Task<StudentDTO> GetByIDAsync(int id)
        {
            return Mapper.Map.EntityToDto(await _repository.GetByIDAsync(id));
        }
        public async Task<Response<StudentDTO>> UpdateAsync(StudentDTO model)
        {
            return await Task.Run(async () =>
            {
                Response<StudentDTO> response = new Response<StudentDTO>();
                response.Success = false;

                var pnAlreadyExist = await ValidatePNAsync(model.PN, model.Id);
                if (pnAlreadyExist)
                    response.Message = "პირადი ნომრით უზერი უკვე არსებობს \n";


                var ageIsAbove16 = await ValidateAgeLessThan16Async(model.BirthDate);
                if (!ageIsAbove16)
                    response.Message += "ასაკი ნაკლებია 16 ზე";


              
                if (!pnAlreadyExist && ageIsAbove16)
                {
                    var success = await _repository.UpdateAsync(Mapper.Map.DtoToEntity(model));
                    if (success)
                    {
                        response.Body = model;
                        response.Success = true;
                        return response;
                    }
                }
               
                return response;

               
            });

             
        }

       
    }
}
