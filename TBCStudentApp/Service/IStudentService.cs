﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;

namespace API.Service
{
    public interface IStudentService
    {
        #region Validations
        Task<bool> ValidatePNAsync(string pn, int id);

        Task<bool> ValidateAgeLessThan16Async(DateTime birthdate);
        #endregion


        Task<Response<StudentDTO>> CreateAsync(StudentDTO model);
        Task<bool> DeleteAsync(int id);
        Task<Response<StudentDTO>> UpdateAsync(StudentDTO model);
        Task<StudentDTO> GetByIDAsync(int id);
        Task<List<StudentDTO>> GetAllAsync(string srchtxt, int currentPageIndex);
        Task<PagingSettings> GetPagingSettings(string srchtxt, int currentPageIndex);


    }
}
