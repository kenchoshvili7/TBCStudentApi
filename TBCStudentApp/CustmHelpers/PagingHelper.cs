﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.CustmHelpers
{
    public class PagingHelper
    {

        public static List<StudentDTO> GetOnePageData(List<StudentDTO> result, int? currentPageIndex)
        {
            // row count  on per page
            int rowcount = 2;
            int PageCount = 0;

            if (currentPageIndex.HasValue && result.Any())
            {

                int mteli = result.Count / rowcount;
                int wiladi = result.Count % rowcount;

                PageCount = mteli;
                if (wiladi > 0)
                {
                    PageCount++;
                }

                result = result.Skip(rowcount * (currentPageIndex.Value - 1)).Take(rowcount).ToList();



            }
            return result;
        }


        public static PagingSettings Get_Paging_Settngs(List<StudentDTO> result, int? currentPageIndex)
        {
            PagingSettings settings = new PagingSettings();

            // row count  on per page
            int rowcount = 2;
            int PageCount = 0;

            if (currentPageIndex.HasValue && result.Any())
            {

                int mteli = result.Count / rowcount;
                int wiladi = result.Count % rowcount;

                PageCount = mteli;
                if (wiladi > 0)
                {
                    PageCount++;
                }

                result = result.Skip(rowcount * (currentPageIndex.Value - 1)).Take(rowcount).ToList();


                settings.PageCountList = new List<int>();


                if (PageCount <= 3)
                {
                    Set_First_Perv_Next_Last_VariableValues(settings, PageCount, currentPageIndex);
                    for (int i = 1; i <= PageCount; i++)
                    {
                        settings.PageCountList.Add(i);
                    }
                }
                else
                {
                    Set_First_Perv_Next_Last_VariableValues(settings, PageCount, currentPageIndex);

                    // generate paging items if  PageCount > 10  and  currentPageIndex.Value <= 10
                    if (currentPageIndex.Value <= 3)
                    {
                        for (int i = 1; i <= 3; i++)
                        {
                            settings.PageCountList.Add(i);
                        }
                    }
                    else
                    {


                        int counter = 1;
                        for (int i = currentPageIndex.Value; counter <= 3; i--)
                        {
                            counter++;
                            settings.PageCountList.Add(i);

                        }
                        settings.PageCountList.Reverse();


                    }
                }
            }
            return settings;


        }

        public static void Set_First_Perv_Next_Last_VariableValues(PagingSettings settings, int PageCount, int? currentPageIndex)
        {

            settings.showFirstButtton = true;
            settings.showPrevButton = true;
            settings.showNextButton = true;
            settings.showLastButton = true;



            settings.currentPageIndex = currentPageIndex.Value;
            settings.firstPageIndex = 1;

            if (currentPageIndex.Value > 1)
            { settings.prevPageIndex = currentPageIndex.Value - 1; }
            else
            {
                settings.showFirstButtton = false;
                settings.showPrevButton = false;
                settings.prevPageIndex = 1;
            }

            if (currentPageIndex.Value == PageCount)
            {
                settings.nextPageIndex = -777;
                settings.lastPageIndex = -777;

                settings.showNextButton = false;
                settings.showLastButton = false;

            }
            else
            {
                settings.nextPageIndex = currentPageIndex.Value + 1;

            }

            settings.lastPageIndex = PageCount;

        }


    }
}
